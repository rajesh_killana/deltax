using System.Collections.Generic;
using System.Linq;
using project.Model.DB;
using project.Model.Request;
using project.Model.Response;
using project.Repository;

namespace project.Service
{
    public interface IActorService 
    { 
        public IEnumerable<ActorResponse> Get();
        public ActorResponse Get(int id);
        public void Post(ActorRequest actor);
        public void Put(int id, ActorRequest actor);
        public void Delete(int id);
        public IEnumerable<ActorResponse> GetActorsByMovieId(int movieId);


        }
}