using System.Collections.Generic;
using System.Linq;
using project.Model.Request;
using project.Model.Response;
using project.Repository;

namespace project.Service
{
    public interface IGenreService 
    {   
        public IEnumerable<GenreResponse> Get();
        public GenreResponse Get(int id);
         public void Post(GenreRequest genre);
        public void Delete(int id);
        public void Put(int id,GenreRequest genre);
   
        public IEnumerable<GenreResponse> GetGenreByMovieId(int movieId);
        


        }
}