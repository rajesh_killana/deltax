using System.Collections.Generic;
using System.Linq;
using project.Model.Request;
using project.Model.Response;
using project.Repository;

namespace project.Service
{
    public interface IProducerService
    {
        public IEnumerable<ProducerResponse> Get();
        public ProducerResponse Get(int id);
        public void Post(ProducerRequest Producer);
        public void Delete(int id);
        public void Put(int id,ProducerRequest producer);
    }
}