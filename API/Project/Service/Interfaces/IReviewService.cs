using System.Collections.Generic;
using project.Model.DB;
namespace project.Service
{
    public interface IReviewService
    { 
        public IEnumerable<ReviewResponse> Get(int id);
        public ReviewResponse Get(int movieId,int reviewId);
        public void Post(int movieId,ReviewRequest review);
        public void Put(int movieId,int reviewId, ReviewRequest review);
        public void Delete(int movieId,int reviewId);
        
    }
}