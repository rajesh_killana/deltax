using System.Collections.Generic;
using System.Linq;
using project.Model.DB;
using project.Model.Request;
using project.Model.Response;
using project.Repository;

namespace project.Service
{
    public interface IMovieService 
    {
        public IEnumerable<MovieResponse> Get();
        public MovieResponse Get(int id);
        public void Post(MovieRequest movie);
        public void Put(int id, MovieRequest movie);
        public void Delete(int id);
        
    }
}