using System.Collections.Generic;
using System.Linq;
using project.Model.DB;
using project.Model.Request;
using project.Model.Response;
using project.Repository;

namespace project.Service
{
    public class ActorService : IActorService
    {
        private readonly IActorRepository _actorRepository;
      
        public ActorService(IActorRepository actorRepository)
        {
            _actorRepository = actorRepository;
          
        }
        public IEnumerable<ActorResponse> Get()
        {
            var actors = _actorRepository.Get();
            return actors.Select(a => new ActorResponse
            {
                Id = a.Id,
                Name = a.Name,
                DOB=a.DOB,
                Bio=a.Bio,
                Sex=a.Sex
            });
        }
        public ActorResponse Get(int id)
        {
            var actor = _actorRepository.Get(id);  
            return actor is null ? null : new ActorResponse{
                Id = actor.Id,
                Name = actor.Name,
                DOB=actor.DOB,
                Bio=actor.Bio,
                Sex=actor.Sex
            };
        }
        public void Post(ActorRequest actor){
            Actor actorObject = new Actor
            {
                Id = 0,
                Name = actor.Name,
                DOB = actor.DOB,
                Bio = actor.Bio,
                Sex = actor.Sex
            };
           _actorRepository.Post(actorObject);
            
           
        }
        public void Put(int id,ActorRequest actor){
            var actorObject = new Actor
            {
                Id = id,
                Name = actor.Name,
                DOB = actor.DOB,
                Bio = actor.Bio,
                Sex = actor.Sex
            };
            _actorRepository.Put(actorObject);
        }
        public void Delete(int id){
            _actorRepository.Delete(id);
        }
       
        public IEnumerable<ActorResponse> GetActorsByMovieId(int movieId)
        {
            var actors = _actorRepository.GetActorsByMovieId(movieId);
            return actors.Select(a => new ActorResponse
            {
                Id = a.Id,
                Name = a.Name,
                Bio = a.Bio,
                DOB = a.DOB,
                Sex = a.Sex
            });
        }

    }
}