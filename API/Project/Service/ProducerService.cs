using System.Collections.Generic;
using System.Linq;
using project.Model.DB;
using project.Model.Request;
using project.Model.Response;
using project.Repository;

namespace project.Service
{
    public class ProducerService : IProducerService
    {
        private readonly IProducerRepository _producerRepository;

        public ProducerService(IProducerRepository producerRepository)
        {
            _producerRepository = producerRepository;
        }
        
        public IEnumerable<ProducerResponse> Get()
        {
            var producer = _producerRepository.Get();
            return producer.Select(p => new ProducerResponse
            {
                Id = p.Id,
                Name = p.Name,
                DOB=p.DOB,
                Bio=p.Bio,
                Sex=p.Sex
            });
        }

        public ProducerResponse Get(int id)
        {
            
            var producer = _producerRepository.Get(id);  
            return producer is null ? null : new ProducerResponse{
                Id = producer.Id,
                Name = producer.Name,
                DOB=producer.DOB,
                Bio=producer.Bio,
                Sex=producer.Sex
            };
        }
        public void Post(ProducerRequest producer)
        {
            var producerObject = new Producer
            {
                Id = _producerRepository.Get().Count()+1,
                Name = producer.Name,
                DOB = producer.DOB,
                Bio = producer.Bio,
                Sex = producer.Sex
            };
            _producerRepository.Post(producerObject);
        }
        public void Delete(int id)
        {
            _producerRepository.Delete(id);
        }
        public void Put(int id, ProducerRequest producer)
        {
            var producerObject = new Producer
            {
                Id = id,
                Name = producer.Name,
                DOB =  producer.DOB,
                Bio = producer.Bio,
                Sex = producer.Sex
            };
            _producerRepository.Put(producerObject);
        }
    }
}