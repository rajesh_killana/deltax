using System.Collections.Generic;
using System.Linq;
using project.Model.DB;
using project.Repository;

namespace project.Service
{
    public class ReviewService : IReviewService
    {
        private readonly IReviewRepository _reviewRepository;
        public ReviewService(IReviewRepository reviewRepository)
        {
            _reviewRepository =reviewRepository;
        }
        public IEnumerable<ReviewResponse> Get(int id)
        {
           var review =_reviewRepository.Get(id);
            if (review is null)
            {
                return null;
            }
            else
            { 
                var reviews= review.Select(a => new ReviewResponse
                {
                    Id=a.Id,
                    MId=a.MovieId,
                    Content=a.Content,
                    Rating=a.Rating
                });
                return reviews;
                
            }
            
        }
        public ReviewResponse Get(int movieId, int reviewId)
        {
            var review = _reviewRepository.Get(movieId, reviewId);
            return review is null ? null : new ReviewResponse
            {
                Id=review.Id,
                MId=review.MovieId,
                Content=review.Content,
                Rating=review.Rating
            };
        }
        public void Post(int movieId,ReviewRequest review)
        {
            var reviewobj = new Review
            {
                
                MovieId = movieId,
                Content = review.Content,
                Rating = review.Rating
            };
            _reviewRepository.Post(reviewobj);
        }
        public void Put(int movieId, int reviewId,ReviewRequest review)
        {
            var reviewobj = new Review
            {
                Id=reviewId,
                MovieId=movieId,
                Content=review.Content,
                Rating=review.Rating
            };
            _reviewRepository.Put(reviewobj);
        }
        public void Delete(int movieId,int reviewId){
            _reviewRepository.Delete(movieId, reviewId);
        }

        
    }
}