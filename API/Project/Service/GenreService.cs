using System.Collections.Generic;
using System.Linq;
using project.Model.DB;
using project.Model.Request;
using project.Model.Response;
using project.Repository;

namespace project.Service
{
    public class GenreService : IGenreService
    {
        private readonly IGenreRepository _genreRepository;
  

        public GenreService(IGenreRepository genreRepository)
        {
            _genreRepository = genreRepository;
          
        } 
        public IEnumerable<GenreResponse> Get()
        {
            var genres = _genreRepository.Get();
            return genres.Select(g=> new GenreResponse
            {
                Id = g.Id,
                Name = g.Name,
                
            });
        }
        public GenreResponse Get(int id)
        {
            var genre = _genreRepository.Get(id);  
            return genre is null ? null : new GenreResponse{
                Id = genre.Id,
                Name = genre.Name,
                
            };
        }
        public void Post(GenreRequest genre)
        {
            var genreObject = new Genre
            {
                Id = _genreRepository.Get().Count()+1,
                Name = genre.Name,
            };
            _genreRepository.Post(genreObject);
        }
        public void Delete(int id)
        {
            _genreRepository.Delete(id);
        }
        public void Put(int id, GenreRequest genre)
        {
            var genreObject = new Genre
            {
                Id = id,
                Name = genre.Name,
            };
            _genreRepository.Put(genreObject);
        }
       

        public IEnumerable<GenreResponse> GetGenreByMovieId(int movieId)
        {
            var genre = _genreRepository.GetGenresByMovieId(movieId);
            return genre.Select(g => new GenreResponse
            {
                Id = g.Id,
                Name = g.Name
            });
        }

    }
}