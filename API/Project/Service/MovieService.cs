using System.Collections.Generic;
using System.Linq;
using project.Model.DB;
using project.Model.Request;
using project.Model.Response;
using project.Repository;


namespace project.Service
{
    public class MovieService : IMovieService
    {
        private readonly IMovieRepository _movieRepository;
        private  readonly IProducerService _producerService;
        private  readonly IActorService _actorService;
        private  readonly IGenreService _genreService;
       
        public MovieService(IMovieRepository movieRepository, IActorService actorService, IProducerService producerService, IGenreService genreService)
        {
            _movieRepository = movieRepository;
            _actorService = actorService;
            _producerService = producerService;
            _genreService = genreService;
           
        }       
        public IEnumerable<MovieResponse> Get()
        {
            var movies = _movieRepository.Get();
            return movies.Select(m => new MovieResponse
            {
                Id = m.Id,
                Name = m.Name,
                Year = m.Year,
                Plot = m.Plot,
                Actors = _actorService.GetActorsByMovieId(m.Id),
                Genres = _genreService.GetGenreByMovieId(m.Id),
                Producer = _producerService.Get(m.Producer), 
                CoverPage = m.CoverPage
            });
        }

        public MovieResponse Get(int id)
        {
            var m = _movieRepository.Get(id);
            return m is null ?  null : new MovieResponse
                    {
                Id = m.Id,
                Name = m.Name,
                Year = m.Year,
                Plot = m.Plot,
                Actors= _actorService.GetActorsByMovieId(m.Id),
                Genres = _genreService.GetGenreByMovieId(m.Id),
                Producer = _producerService.Get(m.Producer),
                CoverPage = m.CoverPage
            };  
        }
        public void Post(MovieRequest movie){
            var movieobject=new Movie{
                Name=movie.Name,
                Year=movie.Year,
                Plot= movie.Plot,
                Producer= movie.Producer,
                CoverPage= movie.CoverPage
            };

            _movieRepository.Post(movieobject,string.Join(",", movie.Actors),string.Join(",", movie.Genres));  
        }
        public void Put(int id,MovieRequest movie){
             var movieobject=new Movie{
                Name=movie.Name,
                Year=movie.Year,
                Plot= movie.Plot,
                Producer= movie.Producer,
                CoverPage= movie.CoverPage
            };
            _movieRepository.Put(id,movieobject,string.Join(",", movie.Actors),string.Join(",", movie.Genres));
        }
        public void Delete(int id){
            _movieRepository.Delete(id);
        }
        
    }
}