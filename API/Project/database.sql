
CREATE DATABASE [IMDBassignment] 
USE [IMDBassignment]
GO

CREATE TABLE [dbo].[Actors](
	[id] [int] IDENTITY(1,1) NOT NULL PRIMARY KEY,
	[Name] [varchar](50) NOT NULL,
	[sex] [varchar](7) NOT NULL,
	[DOB] [date] NOT NULL,
	[Bio] [varchar](max) NULL,
)
GO
CREATE TABLE [dbo].[Producers](
	[Id] [int] IDENTITY(1,1) NOT NULL PRIMARY KEY,
	[Name] [varchar](50) NOT NULL,
	[sex] [varchar](7) NOT NULL,
	[DOB] [date] NOT NULL,
	[Bio] [varchar](max) NULL,
)
GO
CREATE TABLE [dbo].[movies](
	[Id] [int] IDENTITY(1,1) NOT NULL PRIMARY KEY,
	[Name] [varchar](50) NOT NULL,
	[Year] [int] NOT NULL,
	[PLot] [varchar](max) NOT NULL,
	[coverpage] [varchar](max) NULL,
	[producer] [int] NOT NULL FOREIGN KEY REFERENCES PRODUCERS(ID) ,
)
CREATE TABLE [dbo].[Genres](
	[id] [int] IDENTITY(1,1) NOT NULL PRIMARY KEY,
	[Name] [varchar](10) NOT NULL,
)
GO

CREATE TABLE [dbo].[GenreMovieMapping](
	[mid] [int] NOT NULL  FOREIGN KEY REFERENCES Movies(id),
	[gid] [int] NOT NULL  FOREIGN KEY REFERENCES Genres(id)
) 
GO

CREATE TABLE [dbo].[MovieMappingActor](
	[aid] [int] NOT NULL  FOREIGN KEY REFERENCES actors(id),
	[mid] [int] NOT NULL  FOREIGN KEY REFERENCES movies(id)
)
GO



CREATE TABLE [dbo].[reviews](
	[id] [int] IDENTITY(1,1) NOT NULL PRIMARY KEY,
	[mid] [int] NOT NULL  FOREIGN KEY REFERENCES movies(id),
	[content] [varchar](max) NULL,
	[rating] [int] NOT NULL,
)


CREATE PROC [dbo].[sp_AddMovie] 
	@Name VARCHAR(100)
    ,@Year INT
    ,@Plot VARCHAR(1000)
    ,@ProducerId INT
    ,@ActorIds VARCHAR(MAX)
    ,@GenreIds VARCHAR(MAX)
    ,@CoverPage NVARCHAR(500)
AS
BEGIN
    --insert into movies table
    INSERT INTO Movies (
        [Name],
		[Year],
        Plot,
		CoverPage,
        Producer  
        )
    VALUES (
        @Name
        ,@Year
        ,@Plot
        ,@CoverPage
		,@ProducerId
        );

    DECLARE @MovieId INT

    SET @MovieId = SCOPE_IDENTITY();
    INSERT INTO MovieMappingActor
    SELECT CAST([value] as INT) [AId]
	,@MovieId [mId]
    FROM string_split(@ActorIds, ',')
    INSERT INTO GenreMovieMapping
    SELECT @MovieId [MId]
        , CAST([value] as INT) [GId]
    FROM string_split(@GenreIds, ',')
END
GO

CREATE PROCEDURE [dbo].[sp_DeleteActor] 
	@Id INT
AS
BEGIN
	DELETE FROM MovieMappingActor WHERE aId=@Id
    Delete actors WHERE id= @Id
END
GO

CREATE PROCEDURE [dbo].[sp_DeleteGenere]
	@Id INT
AS
BEGIN
	DELETE FROM GenreMovieMapping WHERE gId=@Id
    Delete Genres WHERE Id = @Id
END
GO

CREATE PROC [dbo].[sp_DeleteMovie] 
	@MovieId INT
AS
BEGIN
	DELETE FROM MovieMappingActor WHERE mId=@MovieId
    DELETE FROM GenreMovieMapping WHERE mId=@MovieId
	delete from reviews where mid=@MovieId
    Delete movies WHERE Id = @MovieId
END
GO

CREATE PROC [dbo].[sp_deleteproducer]
@Pid int
AS
BEGIN
Declare @Mid int
DECLARE ids CURSOR FOR 
		SELECT id 
		FROM movies 
		WHERE producer=@Pid	
OPEN ids  
FETCH NEXT FROM ids INTO @Mid 
WHILE @@FETCH_STATUS = 0  
BEGIN  
      exec sp_DeleteMovie @Mid 
      FETCH NEXT FROM ids INTO @Mid 
END 

CLOSE ids
	 
	DELETE FROM Producers WHERE Id=@pID
END
GO

CREATE PROC [dbo].[sp_UpdateMovie] 
	@MovieId INT
    ,@Name VARCHAR(100)
    ,@Year INT
    ,@Plot VARCHAR(1000)
    ,@CoverPage NVARCHAR(500)
    ,@ProducerId INT
    ,@ActorIds VARCHAR(MAX)
    ,@GenreIds VARCHAR(MAX)
AS
BEGIN
    UPDATE Movies
    SET [Name] = @Name
        ,[Year] = @Year
        ,Plot = @Plot
        ,Producer = @ProducerId
        ,CoverPage = @CoverPage
    WHERE Id = @MovieId
    DELETE FROM MovieActorMapping WHERE mId=@MovieId
    DELETE FROM GenreMovieMapping WHERE mId=@MovieId
    INSERT INTO MovieActorMapping
    SELECT @MovieId [MovieId]
        ,[value] [ActorId]
    FROM string_split(@ActorIds, ',')
    INSERT INTO GenreMovieMapping
    SELECT @MovieId [MovieId]
        ,[value] [GenreId]
    FROM string_split(@GenreIds, ',')
END
GO
