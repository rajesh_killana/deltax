using project.Service;
using Microsoft.AspNetCore.Mvc;
using project.Model.Request;
namespace project.Controllers
{
    [ApiController]
    [Route("producers")]
    public class ProducerController : ControllerBase
    {
        private readonly IProducerService _producerService;

        public ProducerController(IProducerService producerService)
        {
            _producerService = producerService;
        }

        [HttpGet]
        public IActionResult Get()
        {
            return Ok(_producerService.Get());
        }
        
        [Route("{id}")] 
        [HttpGet]
        public IActionResult Get(int id)
        { 
         var producer =_producerService.Get(id);
            return producer is null ? NotFound() : Ok(producer);  
            
        }
        [HttpPost]
        public IActionResult Post([FromBody]ProducerRequest producer)    
        {
            _producerService.Post(producer);
            return Ok("Added  successfully");
        }
        [Route("{id}")]
        [HttpDelete]
        public IActionResult Delete(int id)
        {
          
            _producerService.Delete(id);
            return Ok("Deleted successfully");  
            
        }
        [Route("{id}")]
        [HttpPut]
        public IActionResult Put(int id, [FromBody] ProducerRequest producer)
        {
            
            _producerService.Put(id, producer);
            return Ok("Update successfully");  
           
        }
    }
}
