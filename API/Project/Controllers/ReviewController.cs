using project.Service;
using Microsoft.AspNetCore.Mvc;
using project.Model.DB;

namespace project.Controllers
{
    [ApiController]
    public class ReviewController : ControllerBase
    {
        private readonly IReviewService _reviewService;

        public ReviewController(IReviewService reviewService)
        {
            _reviewService = reviewService;
        }
        [HttpGet]
        [Route("movies/{id}/reviews")] // movies/1/reviews
        public IActionResult Get(int id)
        {
            var reviews =_reviewService.Get(id);
            return reviews is null ? NotFound() : Ok(reviews); 
        }
        [HttpGet]
        [Route("movies/{movieId}/reviews/{reviewId}")]
        public IActionResult Get(int movieId,int reviewId)
        {
            var reviews = _reviewService.Get(movieId,reviewId);
            return reviews is null ? NotFound() : Ok(reviews);
        }
        [HttpDelete]
        [Route("movies/{movieId}/reviews/{reviewId}")]
        public IActionResult Delete(int movieId,int reviewId)
        {
            _reviewService.Delete(movieId,reviewId);
            return Ok("Deleted successfully");
        }
        [HttpPut]
        [Route("movies/{movieId}/reviews/{reviewId}")]
        public IActionResult Put(int movieId, int reviewId,[FromBody]ReviewRequest review)
        {
            _reviewService.Put(movieId,reviewId, review);
            return Ok("Added  successfully");
        }
        [HttpPost]
        [Route("movies/{id}/reviews")]
        public IActionResult Post(int id,[FromBody] ReviewRequest review)
        {
            _reviewService.Post(id,review);
            return Ok("Update successfully");
        }
    }
}
