using project.Service;
using Microsoft.AspNetCore.Mvc;
using project.Model.Request;

namespace project.Controllers
{
    [ApiController]
    [Route("genres")]
    public class GenreController : ControllerBase
    {
         private readonly IGenreService _genreService;
        public GenreController(IGenreService genreService)
        {
            _genreService = genreService;
        }
        [HttpGet]
        public IActionResult Get()
        {
            return Ok(_genreService.Get());
        }
        [Route("{id}")] 
        [HttpGet]
        public IActionResult Get(int id)
        {
            var genre =_genreService.Get(id);
            return genre is null ? NotFound() : Ok(genre); 
        }
        [HttpPost]
        public IActionResult Post([FromBody]GenreRequest genre)    
        {
            _genreService.Post(genre);
            return Ok("Added  successfully");           
        }
        [Route("{id}")]
        [HttpDelete]
        public IActionResult Delete(int id)
        {
            _genreService.Delete(id);
            return Ok("Deleted successfully");

        }
        [Route("{id}")]
        [HttpPut]
        public IActionResult Put(int id, [FromBody] GenreRequest genre)
        {
            _genreService.Put(id,genre);
            return Ok("Update successfully");

        }
    }
}
