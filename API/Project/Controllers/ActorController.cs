using project.Service;
using Microsoft.AspNetCore.Mvc;
using project.Model.Request;
namespace project.Controllers
{
    [ApiController]
    [Route("actors")]
    public class ActorController : ControllerBase
    {   
        private readonly IActorService _actorService;
        public ActorController(IActorService actorService)
        {
            _actorService = actorService;
        }
        [HttpGet]
        public IActionResult Get()
        {
            return Ok(_actorService.Get());
        }
        [Route("{id}")] 
        [HttpGet]
        public IActionResult Get(int id)
        { 
            var actor =_actorService.Get(id);
            return actor is null ? NotFound() : Ok(actor);   
            
        }
        [HttpPost]
         public IActionResult Post([FromBody]ActorRequest actor){
            _actorService.Post(actor);
            return Ok("add actor sucessfully");   
        }
        [HttpPut("{id}")]
        public IActionResult Put(int id,[FromBody]ActorRequest actor){
            _actorService.Put(id,actor);
            return Ok("update sucessfully");  
             
        } 
        [HttpDelete("{id}")]
         public IActionResult Delete(int id){
            
            _actorService.Delete(id);
            return Ok("deleted sucessfully");  
           
        }
    }
}
