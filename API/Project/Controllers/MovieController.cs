using project.Service;
using Microsoft.AspNetCore.Mvc;
using project.Model.Request;
using Firebase.Storage;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;
using System;


namespace project.Controllers
{
    [ApiController]
    [Route("movies")]
    public class MovieController : ControllerBase
    {
        // private readonly IHostingEnvironment _env;
        // private static string ApiKey = "AIzaSyB35UGBYxuFoNPSu-gq7b55Q9MRP8f0Gcc";
        // private static string Bucket = "deltax-api.appspot.com";
        // private static string AuthEmail = "deltaxapi@gmail.com";
        // private static string AuthPassword = "assignment@123";

        private  readonly IMovieService _movieService;
        public MovieController(IMovieService movieService  )
        {
            _movieService = movieService;
            // client= new FireSharp.FirebaseClient(Config);
        }  
        [HttpGet]
        public IActionResult Get()
        {
            return Ok(_movieService.Get());
        }
        [Route("{id}")] 
        [HttpGet]
        public IActionResult Get(int id)
        {
            var movie = _movieService.Get(id);
           return movie is null ? NotFound() : Ok(movie);    
        }
        [HttpPost]
         public IActionResult Post([FromBody]MovieRequest movie){
         
          _movieService.Post(movie);
        return Ok("Added  successfully");                
        }

        [HttpPut("{id}")]
        public IActionResult Put(int id,[FromBody]MovieRequest movie){
            _movieService.Put( id,movie );
            return Ok("Update successfully");             
        }
        
        [HttpPost("upload")]
        public async Task<IActionResult> UploadFile(IFormFile file)
            {
                if (file == null || file.Length == 0)
                    return Content("file not selected");
                var task = await new FirebaseStorage("deltax-api.appspot.com")
                                                    .Child("images")
                                                    .Child(Guid.NewGuid().ToString() + ".png")
                                                    .PutAsync(file.OpenReadStream());
    	        return Ok(task);
            }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id){
            _movieService.Delete(id);
            return Ok("Deleted successfully");    
        }
    }
}
