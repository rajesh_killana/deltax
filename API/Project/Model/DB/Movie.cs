using System.Collections.Generic;
namespace project.Model.DB
{
    public class Movie
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Year { get; set; }
        public string Plot { get; set; }
        public int Producer{get; set;}
        public string CoverPage { get; set; } 
    }
}