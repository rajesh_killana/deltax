using System;

namespace project.Model.DB
{
    public class Producer
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime DOB { get; set; }
        public string Sex{get; set;}
        public string Bio { get; set; } 
       
    }
}