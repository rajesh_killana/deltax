using System;
using System.ComponentModel.DataAnnotations;

namespace project.Model.Request
{
    public class ActorRequest
    {
        [Required(ErrorMessage = "Name is required")]
        [RegularExpression(".*[a-zA-z]+.*", ErrorMessage ="enter a valid name")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Date of birth is required")]
         public DateTime DOB { get; set; }
        [Required(ErrorMessage = "sex is required")]
        public string Sex{get; set;}
        [Required(ErrorMessage = "Biography is required")]
        public string Bio { get; set; }
    }
}