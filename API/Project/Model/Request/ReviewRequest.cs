using System;

namespace project.Model.DB
{
    public class ReviewRequest
    {
        public string Content{get; set;}
        public int Rating { get; set; }  
    }
}