using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;
using project.Model.Response;

namespace project.Model.Request
{
    public class MovieRequest
    { 
     
        public string Name { get; set; }
 
        public int Year { get; set; }
       
        public string Plot { get; set; }
        public IEnumerable<int> Actors{get; set;}
        public IEnumerable<int> Genres{get; set;}
        public int Producer{get; set;}
        public string CoverPage { get; set; }
    }
}