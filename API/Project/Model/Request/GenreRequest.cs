using System.ComponentModel.DataAnnotations;

namespace project.Model.Request
{
    public class GenreRequest
    {
        [Required (ErrorMessage="Name is required")]
        [RegularExpression(".*[a-zA-z]+.*", ErrorMessage ="enter a valid name")]
        public string Name { get; set; }
    }
}