using System;

namespace project.Model.Response
{
     public class ActorResponse
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime DOB { get; set; }
        public string Sex{get; set;}
        public string Bio { get; set; } 

    }
}