using System;

namespace project.Model.DB
{
    public class ReviewResponse
    {
        public int Id { get; set; }
        
        public int MId { get; set; }
        public string Content{get; set;}
        public int Rating { get; set; } 
    }
}