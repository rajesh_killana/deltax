using System.Collections.Generic;
using project.Model.DB;

namespace project.Model.Response
{
    public class MovieResponse
    {
        public int Id { get; set; }
       public string Name { get; set; }
        public int Year { get; set; }
        public string Plot { get; set; }
        public IEnumerable<ActorResponse> Actors{get; set;} 
        public IEnumerable<GenreResponse> Genres{get; set;}
        public ProducerResponse Producer {get; set;}
        public string CoverPage{ get; set; }
    }
}