using System.Collections.Generic;
using Microsoft.Extensions.Options;
using project.Model.DB;
namespace project.Repository
{
    
  public class ActorRepository : GenericRepository<Actor>,IActorRepository
    {

        public ActorRepository(IOptions<Connection> connection) : base(connection)
        {  
        }
        public IEnumerable<Actor> Get()
        {
            const string sql = @"SELECT * 
                                FROM Actors";
            return base.GetAll(sql);
        }
        public Actor Get(int id)
        {
            const string  sql = @"SELECT * FROM Actors WHERE Id = @id";
           return base.Get(sql,new { @id=id });
        }
        public void Post(Actor actor)
        {
           const string  sql= @"INSERT INTO Actors (NAME,Sex,DOB,BIO ) VALUES (@name,@sex,@dob,@bio) ;" ;
            base.ExecuteQuery(sql, new { @name = actor.Name, @sex = actor.Sex, @dob = actor.DOB, @bio = actor.Bio });
        }
        public void Delete(int id)
        {  
           
           var sql = @"DELETE FROM MovieMappingActor WHERE AId = @Id;
                                DELETE FROM Actors WHERE Id = @Id;";
            base.Delete(sql,new { @id=id});

        }
        public void Put(Actor actor)
        {
         const string  sql=@"UPDATE Actors
                            SET name=@name,sex=@sex,dob=@dob,bio=@bio
                            WHERE @id=id";  
         base.ExecuteQuery(sql,new{@id=actor.Id,@name=actor.Name,@sex=actor.Sex,@dob=actor.DOB,@bio=actor.Bio});
        }
      
        public IEnumerable<Actor> GetActorsByMovieId(int movieId)
        {
            const string sql = @"SELECT A.* FROM Actors A INNER JOIN MovieMappingActor MA ON A.Id=MA.aid WHERE MA.MId=@MovieId";
            return base.GetMany(sql, new { @MovieId = movieId });    
        }
  }
    
}