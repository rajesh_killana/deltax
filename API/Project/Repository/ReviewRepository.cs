using System.Collections.Generic;
using System.Data;
using Dapper;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Options;
using project.Model.DB;

namespace project.Repository
{
    
        public class ReviewRepository : GenericRepository<Review>,IReviewRepository
        {
       
    
        public ReviewRepository(IOptions<Connection> connection):base(connection)
        {
         
        }
        public IEnumerable<Review> Get(int id)
        {
            const string sql = @"SELECT * FROM Reviews WHERE MId = @id";
           return base.GetMany(sql,new { @id = id });
        }
        public Review Get(int movieId, int reviewId)
        {
            const string sql = @"SELECT * FROM Reviews WHERE MId = @movieId AND ID =@reviewId";
        
            return base.Get(sql, new { @movieId = movieId, @reviewId = reviewId });
        }

        public void Post(Review review)
        {
           const string  sql= @"INSERT INTO Reviews (mid,content,rating )VALUES (@mid,@content,@rating) ;" ;
           base.ExecuteQuery(sql,new{@mid=review.MovieId,@content=review.Content,@rating=review.Rating});
        }
        public void Delete(int movieId,int reviewId)
        {
          
           const string  sql=@"DELETE FROM reviews WHERE id=@reviewId AND mid=@movieId;";
            base.ExecuteQuery(sql,new{@reviewId=reviewId,@movieId=movieId});
        }
        public void Put(Review review)
        {
         const string  sql=@"UPDATE Reviews
                            SET mid=@mid,content=@content,rating=@rating
                            WHERE @id=id";  
         base.ExecuteQuery(sql,new{@id=review.Id,@mid=review.MovieId,@content=review.Content,@rating=review.Rating});
        }

       
    }
}