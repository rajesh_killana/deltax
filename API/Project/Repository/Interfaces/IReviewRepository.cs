using System.Collections.Generic;
using System.Data;
using Dapper;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Options;
using project.Model.DB;

namespace project.Repository
{
    
        public interface IReviewRepository
    {
       
        public IEnumerable<Review> Get(int id);
        public Review Get(int movieId, int reviewId);
        public void Post(Review review);
        public void Delete(int movieId, int reviewId);
        public void Put(Review review);
    }
}