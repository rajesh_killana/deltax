using System.Collections.Generic;
using project.Model.DB;

namespace project.Repository
{
    public interface IProducerRepository 
    {
        public IEnumerable<Producer> Get();

        public Producer Get(int id);
        public void Post(Producer producer);
        public void Delete(int id);
        public void Put( Producer producer);
    }
}