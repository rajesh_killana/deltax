using System.Collections.Generic;
using System.Linq;
using project.Model.DB;

namespace project.Repository
{
    public interface IGenreRepository 
    {
        public IEnumerable<Genre> Get();

        public Genre Get(int id);
        public void Post(Genre genre);
        public void Delete(int id);
        public void Put(Genre genre);
        public IEnumerable<Genre> GetGenresByMovieId(int movieId);
        


        }
}