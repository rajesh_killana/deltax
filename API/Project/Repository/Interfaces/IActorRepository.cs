using System.Collections.Generic;
using project.Model.DB;
using project.Model.Request;
using project.Model.Response;

namespace project.Repository
{
    public interface IActorRepository 
    {
        public IEnumerable<Actor> Get();
        public Actor Get(int id);
        public void Post(Actor actor);
        public void Put(Actor actor);
        public void Delete(int id);
      
        public IEnumerable<Actor> GetActorsByMovieId(int movieId);
        
        }
}