﻿using System.Collections.Generic;
namespace project.Repository
{
    public interface IGenericRepository<TClass> where TClass : class
    {
        public IEnumerable<TClass> GetAll(string query);
        public TClass Get(string query, object ob);
        public IEnumerable<TClass> GetMany(string query, object ob);
        public void Delete(string query, object ob);
        public void ExecuteProcedure(string procedureName, object ob);
        public void ExecuteQuery(string procedureName, object ob);
       
    }
}
