using System.Collections.Generic;
using project.Model.DB;
using project.Model.Request;

namespace project.Repository
{
    public interface IMovieRepository 
    {
        public IEnumerable<Movie> Get();
        public Movie Get(int id);
        public void Post(Movie movie,string actorids, string genreids);
        public void Put(int id,Movie movie,string actorids, string genreids);
        public void Delete(int id);
    }
}