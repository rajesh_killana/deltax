using System.Collections.Generic;
using Microsoft.Extensions.Options;
using project.Model.DB;

namespace project.Repository
{
    
        public class GenreRepository : GenericRepository<Genre>,IGenreRepository
        {
        public GenreRepository(IOptions<Connection> connectionString) : base(connectionString)
        {
        }

        public IEnumerable<Genre> Get()
        {
            const string  sql = @"SELECT * FROM genres";
            return base.GetAll(sql);
        }
        public Genre Get(int id)
        {
            const string  sql = @"SELECT * FROM genres WHERE Id = @id";
           return base.Get(sql, new { @id = id });
        }
        public void Post(Genre genre)
        {
           const string  sql= @"INSERT INTO Genres (NAME ) VALUES (@name) ;" ;
            base.ExecuteQuery(sql, new { @name = genre.Name });
        }
        public void Delete(int id)
        {
           
            const string sql = @"DELETE FROM MovieGenreMapping WHERE GenreId = @Id;
                                DELETE FROM Genres WHERE Id = @Id;";
;            base.ExecuteQuery(sql, new { @id=id});
        }
        public void Put(Genre genre)
        {
         const string  sql=@"UPDATE genre
                            SET name=@name
                            WHERE @id=id";
         base.ExecuteQuery(sql, new { @id = genre.Id, @name = genre.Name });
        }
      
        public IEnumerable<Genre> GetGenresByMovieId(int movieId)
        {
            const string sql = @"SELECT G.* FROM Genres G INNER JOIN GENREMOVIEMapping  GM ON G.Id=GM.GId WHERE GM.MId=@MovieId";
            return base.GetMany(sql, new { @MovieId = movieId });
        }
    }

}