using System;
using System.Collections.Generic;
using System.Data;
using Dapper;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Options;
using project.Model.DB;
using project.Model.Request;

namespace project.Repository
{
    public class MovieRepository : GenericRepository<Movie>,IMovieRepository
    {
        public MovieRepository(IOptions<Connection> connection):base(connection)
        { 
        } 
        public IEnumerable<Movie> Get()
        {
             const string  sql = @"SELECT * FROM movies";  
            return base.GetAll(sql);  
        }
        public Movie Get(int id)
        {
             const string  sql = @"SELECT * FROM movies WHERE Id = @id";
           return base.Get(sql, new { @id = id }); 
        }
        public void Post(Movie movie,string actorIds, string genreIds){
            base.ExecuteProcedure("usp_AddMovie",new{@Name=movie.Name,@Year=movie.Year,@Plot=movie.Plot,@producerId=movie.Producer,@actorids=actorIds,@GenreIds=genreIds,@CoverPage=movie.CoverPage} );
        }
        public void Put(int id,Movie movie,string actorIds, string genreIds){
           base.ExecuteProcedure("usp_UpdateMovie",new{@Movieid=id,@Name=movie.Name,@Year=movie.Year,@Plot=movie.Plot,@producerId=movie.Producer,@actorids=actorIds,@GenreIds=genreIds,@CoverPage=movie.CoverPage} );
        }
        public void Delete(int id){
            base.ExecuteProcedure("usp_DeleteMovie",new {@Movieid=id});    
        }
    }
}