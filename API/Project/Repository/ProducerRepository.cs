using System.Collections.Generic;
using Microsoft.Extensions.Options;
using project.Model.DB;

namespace project.Repository
{
    public class ProducerRepository : GenericRepository<Producer>,IProducerRepository
    {
       
        public ProducerRepository(IOptions<Connection> connection) :base(connection)
        {
        }
        public IEnumerable<Producer> Get()
        {
             const string  sql = @"SELECT * FROM producers";
            return base.GetAll(sql);
        }

        public Producer Get(int id)
        {
            const string  sql = @"SELECT * FROM Producers WHERE Id = @id";
           return base.Get(sql, new { @id = id });
        }
        public void Post(Producer producer)
        {
            const string  sql= @"INSERT INTO Producers (NAME,Sex,DOB,BIO ) VALUES (@name,@sex,@dob,@bio) ;" ;
           base.ExecuteQuery(sql,new{@name=producer.Name,@sex=producer.Sex,@dob=producer.DOB,@bio=producer.Bio});
        }
        public void Delete(int id)
        {
              base.ExecuteProcedure("usp_Deleteproducer",new{@pid=id});
        }
        public void Put( Producer producer)
        {
            const string  sql=@"UPDATE producers
                            SET name=@name,sex=@sex,dob=@dob,bio=@bio
                            WHERE @id=id";  
         base.ExecuteQuery(sql,new{@id=producer.Id,@name=producer.Name,@sex=producer.Sex,@dob=producer.DOB,@bio=producer.Bio});
        }
    }
}