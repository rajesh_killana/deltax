﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Moq;
using project.Model.DB;
using project.Repository;

namespace ProjectTest.MockResources
{
    public class ReviewMock
    {
        public static readonly Mock<IReviewRepository> ReviewRepoMock = new Mock<IReviewRepository>();

        private static List<Review> ListOfReviews = new List<Review>
        {
            new Review
            {
                Id = 1,
                MovieId=1,
                Content="best",
               Rating=4               
                },
            new Review
            {
              Id = 2,
              MovieId=1,
              Content="good",
              Rating=3
            },
            new Review
            {
              Id = 3,
              MovieId=2,
              Content="too good",
              Rating=3
            },
            new Review
            {
              Id = 4,
              MovieId=3,
              Content="one time watch",
              Rating=2
            }
        };
        public static  IEnumerable<Review> reviewlist(int x)
        {
            if (ListOfReviews.Any(r => r.MovieId == x))
            {
                return ListOfReviews.Where(r => r.MovieId == x);
            }
            return null;

        }

        public static void MockGet()
        {
            ReviewRepoMock.Setup(m => m.Get(It.IsAny<int>())).Returns((int x) => reviewlist(x) );
            ReviewRepoMock.Setup(m => m.Get(It.IsAny<int>(), It.IsAny<int>())).Returns((int mid, int rid) => ListOfReviews.Where(r=>(r.Id==rid)&&(r.MovieId==mid)).FirstOrDefault());
        }
        public static void MockDelete()
        {
            /*
            ReviewRepoMock.Setup(x => x.Delete(It.IsAny<int>())).Callback((int id) => {
                ListOfReviews.RemoveAll((x) => x.Id == id);
            });
            */

        }
        public static void MockPost()
        { /*
            ReviewRepoMock.Setup(x => x.Post(It.IsAny<Review>())).Callback(new Action<Review>(review => {
                ListOfReviews.RemoveAll(a => a.Id == review.Id);
                ListOfReviews.Add(review);
            }));
            */
        }
        public static void MockPut()
        {/*
            ReviewRepoMock.Setup(x => x.Put(It.IsAny<Review>())).Callback(new Action<Review>(review => {
                dynamic maxid = ListOfReviews.Last().Id;
                dynamic nextid = maxid + 1;
                review.Id = nextid;
                ListOfReviews.Add(review);
            }));
            */
        }

    }
}
