﻿using System;
using System.Collections.Generic;
using System.Linq;
using Moq;
using project.Model.DB;
using project.Model.Response;
using project.Repository;

namespace ProjectTest.MockResources
{
    public class ProducerMock
    {
        public static readonly Mock<IProducerRepository> ProducerRepoMock = new Mock<IProducerRepository>();

        public static List<Producer> ListOfProducers = new List<Producer>
        {
            new Producer
            {
                Id = 1,
                Name = "Mock Producer 1",
                Bio = "--",
                DOB = new DateTime(2015, 12, 31),
                Sex="Male"
            },
            new Producer
            {
                Id = 2,
                Name = "Mock Producer 2",
                Bio = "--",
                DOB = new DateTime(2016, 02, 16),
                Sex="Female"
            }
        };

        public static void MockGet()
        {
            ProducerRepoMock.Setup(x => x.Get()).Returns(ListOfProducers);
            ProducerRepoMock.Setup(x => x.Get(It.IsAny<int>())).Returns((int id) => ListOfProducers.Where(x => x.Id == id).FirstOrDefault());
        }
        public static void MockDelete()
        {
            /*
            ProducerRepoMock.Setup(x => x.Put(It.IsAny<Producer>())).Callback(new Action<Producer>(producer => {
                ListOfProducers.RemoveAll(a => a.Id == producer.Id);
               
            }));
             */

        }
        public static void MockPut()
        {/*
           ProducerRepoMock.Setup(x => x.Put(It.IsAny<Producer>())).Callback(new Action<Producer>(producer => {
               ListOfProducers.RemoveAll(p => p.Id == producer.Id);
               ListOfProducers.Add(producer);
           }));
             */
        }
        public static void MockPost()
        {/*
            ProducerRepoMock.Setup(x => x.Post(It.IsAny<Producer>())).Callback(new Action<Producer>(producer =>
            {
                dynamic maxid = ListOfProducers.Last().Id;
                dynamic nextid = maxid + 1;
                producer.Id = nextid;
                ListOfProducers.Add(producer);
            }));

            */
        }



    }
}