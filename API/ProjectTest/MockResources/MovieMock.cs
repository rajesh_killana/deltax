﻿using System;
using System.Collections.Generic;
using System.Linq;
using Moq;
using project.Model.DB;
using project.Repository;
using ProjectTest.Test.MockResources;

namespace ProjectTest.MockResources
{
    public class MovieMock
    {
        public static readonly Mock<IMovieRepository> MovieRepoMock = new Mock<IMovieRepository>();
        private static List<Movie> ListofMovies = new List<Movie>
        {
            new Movie
            {
                Id = 1,
                Name = "Movie 1",
                Year=1999,
                Plot = "telugu movie",
                Producer =1,
                CoverPage ="movie1.png"
                
            },
            new Movie
            {
                Id = 2,
                Name = "Movie 2",
                Year=2000,
                Plot = "hindi movie",
                Producer =1,
                CoverPage ="movie2.png"
            }
        };
        public static Dictionary<int, List<int>> MovieActor = new Dictionary<int, List<int>>
        {
               { 1, new List<int>{1,2} } ,
            { 2,new List<int>{2} }
        };
        public static Dictionary<int, List<int>> MovieGenre = new Dictionary<int, List<int>>
        {
               { 1, new List<int>{1,3} },
                {2, new List<int>{ 2,4} }
        };
        public static void MockGet()
        {
            ActorMock.ActorRepoMock.Setup(a => a.GetActorsByMovieId(It.IsAny<int>()))
                .Returns((int i) => ActorMock.ListOfActors
                .Where(x => MovieActor.SingleOrDefault(p => p.Key == i).Value.Contains(x.Id)));
            GenreMock.GenreRepoMock.Setup(g => g.GetGenresByMovieId(It.IsAny<int>()))
                .Returns((int i) => GenreMock.ListofGenres
                .Where(x => MovieGenre.SingleOrDefault(p => p.Key == i).Value.Contains(x.Id)));

            ProducerMock.ProducerRepoMock.Setup(m => m.Get(It.IsAny<int>()))
                .Returns((int id) => ProducerMock.ListOfProducers.Where(p=>p.Id==id).SingleOrDefault());
            MovieRepoMock.Setup(x => x.Get()).Returns(ListofMovies);
            MovieRepoMock.Setup(x => x.Get(It.IsAny<int>())).Returns((int id) => ListofMovies.Where(x => x.Id == id).FirstOrDefault());
        }

        public static void MockDelete()

        {

            /*

            MovieRepoMock.Setup(x => x.Delete(It.IsAny<int>())).Callback(new Action<int>((id) => {

                ListofMovies.RemoveAll((x) => x.Id == id);

                MovieActor.Remove(id);

                MovieGenre.Remove(id);
                

            }));

            */

        }

        public static void MockPut()

        {

            /* 

            MovieRepoMock.Setup(x => x.Put(It.IsAny<int>(),It.IsAny<Movie>(), It.IsAny<string>(),It.IsAny<string>())).Callback(new Action <int,Movie,string,string>((id,movie,actorids, genreids) => {

                ListofMovies.RemoveAll(a => a.Id == id);

                MovieActor.Remove(id);

                MovieGenre.Remove(id);

                ListofMovies.Add(movie);

                MovieActor.Add(movie.Id, actorids.Split(',').Select(Int32.Parse).ToList());

                MovieGenre.Add(movie.Id, genreids.Split(',').Select(Int32.Parse).ToList());

                 }));

            */



        }

        public static void MockPost()

        {/*

             MovieRepoMock.Setup(x => x.Post(It.IsAny<Movie>(),It.IsAny<string>(),It.IsAny<string>())).Callback(new Action<Movie,string,string>((movie,actorids,genreids) => {

              dynamic maxid = ListofMovies.Last().Id;

              dynamic nextid = maxid + 1;

            movie.Id = nextid;

            ListofMovies.Add(movie);

            MovieActor.Add(movie.Id, actorids.Split(',').Select(Int32.Parse).ToList());

            MovieGenre.Add(movie.Id, genreids.Split(',').Select(Int32.Parse).ToList());

             }));

           */

        }
    }
}
