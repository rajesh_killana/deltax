﻿using System;
using System.Collections.Generic;
using System.Linq;
using Moq;
using project.Model.DB;
using project.Model.Response;
using project.Repository;

namespace ProjectTest.Test.MockResources
{
    public class ActorMock
    {
        /// <summary>
        /// See how we are using Moq - https://github.com/moq/moq4
        /// </summary>
        public static readonly Mock<IActorRepository> ActorRepoMock = new Mock<IActorRepository>();

        public  static List<Actor> ListOfActors = new List<Actor>
        {
            new Actor
            {
                Id = 1,
                Name = "Mock Actor 1",
                Bio = "--",
                DOB = new DateTime(2015, 12, 31),
                Sex="Male"
            },
            new Actor
            {
                Id = 2,
                Name = "Mock Actor 2",
                Bio = "--",
                DOB = new DateTime(2016, 02, 16),
                Sex="Female"
            }
        };

        public static void MockGet()
        {
            ActorRepoMock.Setup(x => x.Get()).Returns(ListOfActors);
            ActorRepoMock.Setup(x => x.Get(It.IsAny<int>())).Returns((int id) => ListOfActors.Where(x => x.Id == id).FirstOrDefault());
        }

        public static void MockDelete()
        {
            /*
            ActorRepoMock.Setup(x => x.Delete(It.IsAny<int>())).Callback((int id) => {
                ListOfActors.RemoveAll((x) => x.Id == id);
            });
            */
        }
        public static void MockPut()
        {
            /*
           ActorRepoMock.Setup(x => x.Put(It.IsAny<Actor>())).Callback(new Action <Actor>(actor => {
               ListOfActors.RemoveAll(a => a.Id == actor.Id);
               ListOfActors.Add(actor);
           })  );
            */
        }
        public static void MockPost()
        {
            /* ActorRepoMock.Setup(x => x.Post(It.IsAny<Actor>())).Callback(new Action<Actor>(actor => {
                dynamic maxid = ListOfActors.Last().Id;
              dynamic nextid = maxid + 1;
            actor.Id = nextid;
            ListOfActors.Add(actor);
            }));
            */
        }
    }
}
