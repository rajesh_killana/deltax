﻿using System;
using System.Collections.Generic;
using System.Linq;
using Moq;
using project.Model.DB;
using project.Model.Response;
using project.Repository;

namespace ProjectTest.MockResources
{
    public class GenreMock
    {
        public static readonly Mock<IGenreRepository> GenreRepoMock = new Mock<IGenreRepository>();

        public static List<Genre> ListofGenres = new List<Genre>
        {
            new Genre
            {
                Id=1,
                Name="comedy"
               
            },
            new Genre
            {
                 Id=2,
                Name="horror"

            },
             new Genre
            {
                 Id=3,
                Name="suspense"
            }
        };

        public static void MockGet()
        {
            GenreRepoMock.Setup(x => x.Get()).Returns(ListofGenres);
            GenreRepoMock.Setup(x => x.Get(It.IsAny<int>())).Returns((int id) => ListofGenres.Where(x => x.Id == id).FirstOrDefault());
        }
        public static void MockPut()
        {

        }
        public static void MockPost()
        {
        }
        public static void MockDelete()
        {
        }
    }
}
