﻿Feature: Review Resource

Scenario: Get Movie Reviews by movieId
	Given I am a client
	When I make GET Request '/movies/2/reviews'
	Then response code must be '200'
	And response data must look like '[{"id":3,"mId":2,"content":"too good","rating":3}]'

Scenario: Get Review by movieId and reviewId
	Given I am a client
	When I make GET Request '/movies/2/reviews/3'
	Then response code must be '200'
	And response data must look like '{"id":3,"mId":2,"content":"too good","rating":3}'


Scenario: InValid  Movie Reviews by Id
	Given I am a client
	When I make GET Request '/movies/10/reviews'
	Then response code must be '404'


Scenario: ADD Review into the list
		Given I am a client
		When I am making a post request to '/movies/1/reviews' with the following Data '{"content":"best","rating":3}'
		Then response code must be '200'
		
Scenario: Update Review in the list
		Given I am a client
		When I make PUT Request 'movies/1/reviews/1' with the following Data with the following Data '{"content":"best movie","rating":3}'
		Then response code must be '200'
