﻿Feature: Genre Resource
Scenario: Get Genre List
	Given I am a client
	When I make GET Request '/genres'
	Then response code must be '200'
	And response data must look like '[{"id":1,"name":"comedy"},{"id":2,"name":"horror"},{"id":3,"name":"suspense"}]'

Scenario: Get Valid Genre by Id
	Given I am a client
	When I make GET Request '/genres/1'
	Then response code must be '200'
	And response data must look like '{"id":1,"name":"comedy"}'

Scenario: InValid Genre by Id
	Given I am a client
	When I make GET Request '/genres/20'
	Then response code must be '404'

Scenario: ADD Genre into the list
		Given I am a client
		When I am making a post request to '/genres' with the following Data '{"name":"rom-com"}'
		Then response code must be '200'
		
Scenario: Update Genre in the list
		Given I am a client
		When I make PUT Request '/genres/3' with the following Data with the following Data '{"name":"love"}'
		Then response code must be '200'
