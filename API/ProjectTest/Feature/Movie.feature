﻿Feature: Movie Resource
Scenario: Get Movie by id
	Given I am a client
	When I make GET Request '/movies/1'
	Then response code must be '200'
	And response data must look like '{"id":1,"name":"Movie 1","year":1999,"plot":"telugu movie","actors":[{"id":1,"name":"Mock Actor 1","dob":"2015-12-31T00:00:00","sex":"Male","bio":"--"},{"id":2,"name":"Mock Actor 2","dob":"2016-02-16T00:00:00","sex":"Female","bio":"--"}],"genres":[{"id":1,"name":"comedy"},{"id":3,"name":"suspense"}],"producer":{"id":1,"name":"Mock Producer 1","dob":"2015-12-31T00:00:00","sex":"Male","bio":"--"},"coverPage":"movie1.png"}'
Scenario: Get Movie List
	Given I am a client
	When I make GET Request '/movies'
	Then response code must be '200'
	And response data must look like '[{"id":1,"name":"Movie 1","year":1999,"plot":"telugu movie","actors":[{"id":1,"name":"Mock Actor 1","dob":"2015-12-31T00:00:00","sex":"Male","bio":"--"},{"id":2,"name":"Mock Actor 2","dob":"2016-02-16T00:00:00","sex":"Female","bio":"--"}],"genres":[{"id":1,"name":"comedy"},{"id":3,"name":"suspense"}],"producer":{"id":1,"name":"Mock Producer 1","dob":"2015-12-31T00:00:00","sex":"Male","bio":"--"},"coverPage":"movie1.png"},{"id":2,"name":"Movie 2","year":2000,"plot":"hindi movie","actors":[{"id":2,"name":"Mock Actor 2","dob":"2016-02-16T00:00:00","sex":"Female","bio":"--"}],"genres":[{"id":2,"name":"horror"}],"producer":{"id":1,"name":"Mock Producer 1","dob":"2015-12-31T00:00:00","sex":"Male","bio":"--"},"coverPage":"movie2.png"}]'

Scenario: InValid movie by Id
	Given I am a client
	When I make GET Request '/movies/20'
	Then response code must be '404'

Scenario: ADD Movie into the list
	Given I am a client
	When I am making a post request to '/movies' with the following Data '{"name":"new movie 1","year":1989,"plot":"telugu movie","actors":[1,2],genres:[1,3],"producer":1}'
	Then response code must be '200'
		
Scenario: Update Movie in the list
	Given I am a client
	When I make PUT Request '/movies/3' with the following Data with the following Data '{"name":"new movie","year":1997,"plot":"telugu movie","actors":[1,2],genres:[1,3],"producer":1}'
	Then response code must be '200'

