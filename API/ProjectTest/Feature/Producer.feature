﻿Feature: Producer Resource
Scenario: Get Producer List
	Given I am a client
	When I make GET Request '/producers'
	Then response code must be '200'
	And response data must look like '[{"id":1,"name":"Mock Producer 1","dob":"2015-12-31T00:00:00","sex":"Male","bio":"--"},{"id":2,"name":"Mock Producer 2","dob":"2016-02-16T00:00:00","sex":"Female","bio":"--"}]'

Scenario: Get Valid Producer by Id
	Given I am a client
	When I make GET Request '/producers/1'
	Then response code must be '200'
	And response data must look like '{"id":1,"name":"Mock Producer 1","dob":"2015-12-31T00:00:00","sex":"Male","bio":"--"}'

Scenario: InValid Producer by Id
	Given I am a client
	When I make GET Request '/producers/20'
	Then response code must be '404'

Scenario: ADD Producer into the list
		Given I am a client
		When I am making a post request to '/producers' with the following Data '{"name":"Mock producer 4","dob":"2005-12-31","sex":"Male","bio":"--"}'
		Then response code must be '200'
		
Scenario: Update Producer in the list
		Given I am a client
		When I make PUT Request '/producers/3' with the following Data with the following Data '{"name":"ramesh","dob":"2005-12-31","sex":"Male","bio":"--"}'
		Then response code must be '200'
