﻿Feature: Actor Resource
Scenario: Get Actors List
	Given I am a client
	When I make GET Request '/actors'
	Then response code must be '200'
	And response data must look like '[{"id":1,"name":"Mock Actor 1","dob":"2015-12-31T00:00:00","sex":"Male","bio":"--"},{"id":2,"name":"Mock Actor 2","dob":"2016-02-16T00:00:00","sex":"Female","bio":"--"}]'

Scenario: Get Valid Actor by Id
	Given I am a client
	When I make GET Request '/actors/1'
	Then response code must be '200'
	And response data must look like '{"id":1,"name":"Mock Actor 1","dob":"2015-12-31T00:00:00","sex":"Male","bio":"--"}'



Scenario: InValid Actor by Id
	Given I am a client
	When I make GET Request '/actors/20'
	Then response code must be '404'

Scenario: ADD Actor into the list
		Given I am a client
		When I am making a post request to '/actors' with the following Data '{"name":"Mock Actor 3","dob":"2005-12-31","sex":"Male","bio":"--"}'
		Then response code must be '200'
		
Scenario: Update Actor in the list
		Given I am a client
		When I make PUT Request '/actors/3' with the following Data with the following Data '{"name":"rajesh","dob":"2005-12-31","sex":"Male","bio":"--"}'
		Then response code must be '200'
