﻿using Microsoft.Extensions.DependencyInjection;
using TechTalk.SpecFlow;
using ProjectTest.StepFiles;
using ProjectTest.Test.MockResources;
using ProjectTest.MockResources;

namespace ProjectTest.Test.StepFiles
{
    [Scope(Feature = "Producer Resource")]
    [Binding]
    public class ProducerSteps : BaseSteps
    {
        public ProducerSteps(CustomWebApplicationFactory factory)
            : base(factory.WithWebHostBuilder(builder =>
            {
                builder.ConfigureServices(services =>
                {
                    services.AddScoped(_ => ProducerMock.ProducerRepoMock.Object);
                });
            }))
        {
        }


        [BeforeScenario]
        public static void MocksGet()
        {
                ProducerMock.MockGet();
            
        }
        [BeforeScenario]
        public static void MockPost()
        {
            ProducerMock.MockPost();
        }
        [BeforeScenario]
        public static void MockDelete()
        {
            ProducerMock.MockDelete(); 
        }
        [BeforeScenario]
        public static void MockPut()
        {
            ProducerMock.MockPut();  
        }
    }
}