﻿using Microsoft.Extensions.DependencyInjection;
using TechTalk.SpecFlow;
using ProjectTest.StepFiles;
using ProjectTest.Test.MockResources;
using ProjectTest.MockResources;

namespace ProjectTest.StepFiles
{
    
        [Scope(Feature = "Movie Resource")]
        [Binding]
        public class MovieSteps : BaseSteps
        {
            public MovieSteps(CustomWebApplicationFactory factory)
                : base(factory.WithWebHostBuilder(builder =>
                {
                    builder.ConfigureServices(services =>
                    {
                        services.AddScoped(_ => ActorMock.ActorRepoMock.Object);
                        services.AddScoped(_ => ProducerMock.ProducerRepoMock.Object);
                        services.AddScoped(_ => GenreMock.GenreRepoMock.Object);
                        services.AddScoped(_ => MovieMock.MovieRepoMock.Object);
                    });
                }))
            {
            }

            [BeforeScenario]
            public static void MocksGet()
            {
                MovieMock.MockGet();
            }
            [BeforeScenario]
            public static void MockPost()
            {
                MovieMock.MockPost();
            }
            [BeforeScenario]
            public static void MockDelete()
            {
                MovieMock.MockDelete();
            }
            [BeforeScenario]
            public static void MockPut()
            {
                MovieMock.MockPut();
            }
        }
}
