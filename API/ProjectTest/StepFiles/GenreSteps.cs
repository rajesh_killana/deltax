﻿using Microsoft.Extensions.DependencyInjection;
using TechTalk.SpecFlow;
using ProjectTest.MockResources;

namespace ProjectTest.StepFiles
{
    [Scope(Feature = "Genre Resource")]
    [Binding]
    public class GenreSteps : BaseSteps
    {
        public GenreSteps(CustomWebApplicationFactory factory)
            : base(factory.WithWebHostBuilder(builder =>
            {
                builder.ConfigureServices(services =>
                {
                    services.AddScoped(_ => GenreMock.GenreRepoMock.Object);
                });
            }))
        {
        }


        [BeforeScenario]
        public static void MocksGet()
        {
            GenreMock.MockGet();

        }
        [BeforeScenario]
        public static void MockPost()
        {

            GenreMock.MockPost();
        }
        [BeforeScenario]
        public static void MockDelete()
        {

            GenreMock.MockDelete();

        }
        [BeforeScenario]
        public static void MockPut()
        {

            GenreMock.MockPut();

        }
    }
}
