﻿using Microsoft.Extensions.DependencyInjection;
using TechTalk.SpecFlow;
using ProjectTest.StepFiles;
using ProjectTest.Test.MockResources;

namespace ProjectTest.Test.StepFiles
{
    [Scope(Feature = "Actor Resource")]
    [Binding]
    public class ActorSteps : BaseSteps
    {
        public ActorSteps(CustomWebApplicationFactory factory)
            : base(factory.WithWebHostBuilder(builder =>
            {
                builder.ConfigureServices(services =>
                {
                    services.AddScoped(_ => ActorMock.ActorRepoMock.Object);
                });
            }))
        {
        }


        [BeforeScenario]
        public static void MocksGet()
        {
            ActorMock.MockGet();
            
        }
        [BeforeScenario]
        public static void MockPost()
        {
            
            ActorMock.MockPost();
        }
        [BeforeScenario]
        public static void MockDelete()
        {
           
            ActorMock.MockDelete();
           
        }
        [BeforeScenario]
        public static void MockPut()
        {
         
            ActorMock.MockPut();
           
        }
    }
}