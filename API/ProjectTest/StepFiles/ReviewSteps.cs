﻿using Microsoft.Extensions.DependencyInjection;
using TechTalk.SpecFlow;
using ProjectTest.StepFiles;
using ProjectTest.Test.MockResources;
using ProjectTest.MockResources;

namespace ProjectTest.StepFiles
{
    [Scope(Feature = "Review Resource")]
    [Binding]

    public class ReviewSteps : BaseSteps
    {
        public ReviewSteps(CustomWebApplicationFactory factory)
            : base(factory.WithWebHostBuilder(builder =>
            {
                builder.ConfigureServices(services =>
                {
                    services.AddScoped(_ => ReviewMock.ReviewRepoMock.Object);
                });
            }))
        {
        }
        [BeforeScenario]
        public static void Mocks()
        {
            ReviewMock.MockGet();
            ReviewMock.MockPost();
            ReviewMock.MockPut();
            ReviewMock.MockDelete();
        }
    }
}
